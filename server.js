#!/usr/bin/env node
'use strict';

const assert = require('assert');
const mongoose = require('mongoose');
const { Schema, connection} = mongoose;
const DB = 'dbz';
const URI = `mongodb://testdb.insuredmine.info:12017,testdb.insuredmine.info:12019/${DB}`;
const OPTS = { useNewUrlParser: true, useUnifiedTopology: true,replicaSet:"testReplicaDB" };

const schema = new Schema({
  name: String,
});

const Test = mongoose.model('test', schema);

const test = new Test({
  name: 'test',
});

async function run() {
  await mongoose.connect(URI, OPTS);
  await connection.dropDatabase();
  const doc = await Test.create(test);
  assert.ok(doc && doc.name === 'test');
  console.log('All Assertions Pass.');
  await connection.close();
}

run().catch(console.error);